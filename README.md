# ml-ex-3

1st Semester - Machine Learning - Exercise 3

Exercise dataset:
https://github.com/MSc-in-Data-Science/class_material/blob/master/semester_1/Machine_Learning/datasets/digit_recognizer_dataset.csv

Source dataset:
https://www.kaggle.com/c/digit-recognizer
