# Exercise 3 - Results

> System specs:
 - CPU: Intel Core i7, 2,3 GHz, 8 cores
 - RAM: 8GB 1600MHz DDR3

Scoring metric -> **Accuracy**
> *Note: Class distribution is almost balanced - around **4000** instances per class - and this is the reason 'accuracy' is used as a metric.*

## Feature engineering

 1. Columns with zero information removed. **709** features (pixels) remained out of initial **785**
 2. PCA applied (after the *3rd round*) to the dataset in order to further decrease number of features.
 3. No normalisation applied. No significant improvements noted.


## 1st Round - Simple classifier tests
Try a simple classifier (kNN) to get some initial sense of the  accuracy and performance achieved. Also, try various n_jobs (number of cores) and k (for k-fold cross validation) configurations.

> Settings:  
  - KNeighborsClassifier


**Run 1**: 10-fold cv, n_jobs = None

 - Accuracy: 0.967452 (+/- 0.002750), Elapsed time: ~36min

**Run 2**: 5-fold cv, n_jobs = -1

 - Accuracy: 0.965786 (+/- 0.002313), Elapsed time: ~9min

**Run 3**: 3-fold cv, n_jobs = 8

 - Accuracy: 0.964214 (+/- 0.000772), Elapsed time: ~8min


> *Note: In case of n_jobs=8 only 4 out of 8 cores seamed active during the experiment. So for the next runs, n_jobs=-1 setting will be prefered.*


## 2nd Round - Play with various classifiers
Try other classifiers to compare performance. From now on, always use n_jobs=-1 for better performance - use all cores available.

**Run 1**: DecisionTreeClassifier, 10-fold cv

 - Accuracy: 0.859333 (+/- 0.004623), Elapsed time: <1min

**Run 2**: LogisticRegression, 5-fold cv

 - Accuracy: 0.907524 (+/- 0.003368), Elapsed time: 1h+20min

**Run 3**: Gaussian Naive Bayes, 10-fold cv

 - Accuracy: 0.556238 (+/- 0.007963), Elapsed time: <min

**Run 4**: SVM (SVC), 5-fold cv

 - Accuracy: 0.111524 (+/- 0.003157), Elapsed time: 1h+6min

**Run 5**: LDA (Linear Discriminant Analysis), 10-fold cv

 - Accuracy: 0.863929 (+/- 0.003597), Elapsed time: ~2min

**Run 5**: DummyClassifier, 10-fold cv

 - Accuracy: 0.101095 (+/- 0.002828), Elapsed time: <1min



## 3nd Round - PCA
kNN classifier seems to achieve the best score (accuracy) so far. Re-run the algorithm applying PCA before the model training.


**Run 1**: kNN, 3-fold cv, PCA Features: 400

 - Accuracy: 0.964405 (+/- 0.000844), Elapsed time: ~4min

**Run 2**: kNN, 3-fold cv, PCA Features: 100

 - Accuracy: 0.968500 (+/- 0.000456), Elapsed time: ~1.5min

**Run 3**: kNN, 3-fold cv, PCA Features: 50

 - Accuracy: 0.971000 (+/- 0.000518), Elapsed time: ~0.66min

**Run 4**: kNN, 3-fold cv, PCA Features: 25

 - Accuracy: 0.968571 (+/- 0.001138), Elapsed time: ~0.27min

**Run 5**: kNN, 10-fold cv, PCA Features: 50

 - Accuracy: 0.974048 (+/- 0.002613), Elapsed time: ~0.66min

**Discussion:**
> Looks like the *curse of dimensionality* appears in current problem. While in the first steps kNN algorithm achieved very good score (see 1st Round, 10-fold cv -> Accuracy: 0.967452), after applying PCA algorithm with several feature reduction steps (400, 100, 50, 25), score started raising. Around 50 features the score goes above the **0.97** limit. At this round, in *run 5*, using 10-fold cv, a score of **0.974048** is achieved, which is the best so far.


## 4th Round - Evaluate algorithms after PCA
Continue to evaluate all algorithms like in 1st round but with the use of PCA with 50 features:

**Run 1**: DecisionTreeClassifier, 10-fold cv

 - Accuracy: 0.829857 (+/- 0.004889), Elapsed time: 0.25
> Little worse accuracy. Comparable time. No significant gain from the use of PCA.

**Run 2**: LogisticRegression, 10-fold cv, PCA Features: 50

 - Accuracy: 0.897405 (+/- 0.003202), Elapsed time: ~6min
 > Almost the same performance but with significant time gain. PCA to be used in case of large datasets.

**Run 3**: Gaussian Naive Bayes, 10-fold cv

 - Accuracy: 0.870786 (+/- 0.005154), Elapsed time: ~0min
 > Significant performance gain from 0.55 to 0.87. To be noted here that the new PCA features, which are more independent the one from the other, helped NB to perform much better.

**Run 4**: SVM (SVC), 5-fold cv

 - Accuracy: 0.111524 (+/- 0.003805), Elapsed time: 23min
 > SVM cannot solve current problem.

**Run 5**: LDA (Linear Discriminant Analysis), 10-fold cv

 - Accuracy: 0.862881 (+/- 0.005109), Elapsed time: ~0min
 > No significant change with or without PCA. Slightly quicker.


## 5th Round - Grid Search, hyperparameter optimisation
Run a grid search for the kNN classifier (best scores so far) to identify the best parameter (number of neighbors).

> Grid search settings:
 - Estimator: kNN
 - Parameter: n_neighbors (1-10)
 - Scoring: Accuracy
 - Cross validation: 10-fold
 - Jobs: -1 (all available CPUs)

> Best results:
 - Accuracy: **0.974643**
 - Number of neighbors: **3**.

The plot below shows the GridSearchCV results (accuracies over number of neighbors):

![GridSearchCV](GridSearchCV_results.png){#plot}

## 6th Round - Ensembles
Apply ensembles and check accuracy.

### Bagging
Try *BaggingClassifier*.

> Settings:
 - Estimator: KNeighborsClassifier(n_neighbors=3)
 - n_estimators: 100
 - max_samples: 0.5
 - max_features: 0.5
 - n_jobs: -1

 > Results:
  - Accuracy: **0.972333** (+/- 0.002490)
  - Elapsed time: ~15 min

### Boosting
Try *GradientBoostingClassifier*.

> Settings:
 - Estimator: KNeighborsClassifier(n_neighbors=3)
 - n_estimators: 100
 - random_state: 7 (seed)

> Results:
 - Accuracy: **0.967357** (+/- 0.003855)
 - Elapsed time: ~1.3 min


## Summary
To sum-up, it seems that the best score (accuracy: **0.974643**) for the digit recognizer task is achieved using the *KNeighborsClassifier* classifier with the number of neighbors **3**. This result was provided by the *GridSearchCV* function which run for different values of the n_neighbors hyperparameter (1-10). The results can also be previewed in the [plot](#plot) above. What is more, PCA along with parallelisation (*n_jobs*) significantly decreased running time for the fitting process while PCA looks like it avoids the *curse of dimensionality* problem. The number of **50** (initially 709) features selected (after the application of the PCA method), resulted in higher accuracy scores. Finally, ensembles tried produced similar accuracy scores but without outperforming the optimised kNN algorithm.

> Best algorithm: **kNN**
 - Hyperparameter optimisation: Number of neighbors: **3**.
 - Accuracy: **0.974643**
 - Elapsed time: **~0.65min**
